<?php
namespace Travelodge\MS\MailBundle\Tests\ApiContract;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpClient\HttpClient;

/**
 * Class MailControllerTest
 * @package Travelodge\MS\MailBundle\Tests\ApiContract
 */
class MailControllerTest extends WebTestCase
{
    /** @test */
    public function testDefaultFunctionReturn200StatusCode()
    {
        $client = $this->getClient();

        $response = $client->request('GET', '/api/v3/hello');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testDefaultFunctionReturn400StatusCode()
    {
        $client = $this->getClient();

        $response = $client->request('GET', '/api/v3/h');
        $this->assertEquals(400, $response->getStatusCode());
    }


    static function getClient()
    {
        return HttpClient::create(['base_uri' => getenv('API_BASE_URI')]);
    }

}