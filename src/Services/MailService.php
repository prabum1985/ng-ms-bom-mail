<?php

namespace Travelodge\MS\MailBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Swift_Mailer;

/**
 * Class MailService
 * @package Travelodge\MS\MailBundle\Services
 */
class MailService
{
    /** @var EntityManager $em  */
    private $em;

    /** @var \Swift_Mailer $mailer */
    private $mailer; 
    
    /** @var ContainerInterface */
    private $container;

    /**
     * MailService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, 
    \Swift_Mailer $mailer,
    \Twig\Environment $templating, 
    ContainerInterface $container)
    {
        $this->em = $em; 
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->container = $container;
    }
    
    public function saveMember()
    {
        $engine = $this->container->get('templating');
        $content = $engine->render('@TravelodgeMailBundle/Templates/index.html.twig');
        $message = new \Swift_Message;
                $toAddress = ['test456@gmail.com'];
            
                $message
                ->setSubject('test')
                ->setFrom('webmaster@travelodge.co.uk')
                ->setTo($toAddress)
                ->setBody($content, 'text/html');
                $this->mailer->send($message);

        return true;
    }
}
