<?php
namespace Travelodge\MS\MailBundle\Exception;

/**
 * Class MailException
 * @package Travelodge\MS\MailBundle\Exception
 */
class MailException extends \Exception
{

}