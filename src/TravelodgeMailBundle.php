<?php
namespace Travelodge\MS\MailBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class TravelodgeMailBundle
 * @package Travelodge\MS\MailBundle
 */
class TravelodgeMailBundle extends Bundle
{
}