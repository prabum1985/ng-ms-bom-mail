<?php
namespace Travelodge\MS\MailBundle\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class MailController
 * @package Travelodge\MS\MailBundle\Controller
 */
class MailController extends AbstractController
{
    /** @var ValidatorInterface */
    private $validator;

    /** @var LoggerInterface */
    private $logger;

    /**
     * MailController constructor.
     * @param ValidatorInterface $validator
     * @param LoggerInterface $logger
     */
    public function __construct(ValidatorInterface $validator, LoggerInterface $logger)
    {
        $this->validator = $validator;
        $this->logger = $logger;
    }

    /**
     * @Route(path="/sendMail/test", methods={"GET","HEAD"})
     *
     * @param Request $request
     * @param $message
     * @return JsonResponse
     */
    public function indexAction():JsonResponse
    {
        $users = $this->get('ms_bom_mail_service')->saveMember();
        // return json response
        return new JsonResponse(['ok']);
    }

}